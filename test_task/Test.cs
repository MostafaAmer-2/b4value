﻿using System;
using System.Collections.Generic;

namespace test_task
{
    public class Test
    {
        public static void Main (String [] args)
        {
            Console.WriteLine("########## GERMAN ADDRESS FINDER ##########");
            IAddressFinder germanAddressFinder = new germanAddressFinder();
            IAddressFinder universalAddressFinder = new UniversalAddressFinder();
            IAddressManager manager = new Manager(germanAddressFinder, universalAddressFinder);
            IAddressFinder correspondingFinder = manager.GetAddressFinderByCountryCode("Germany");
            IEnumerable<string> test1 = new string[] { "Quincy Happy", "Wacholderweg 52a", "26133 OLDENBURG", "GERMANY" };
            IDictionary<string, string> ouptutDictionary = correspondingFinder.ParseAddress(test1);
            foreach (KeyValuePair<string, string> kvp in ouptutDictionary)
            {
                //textBox3.Text += ("Key = {0}, Value = {1}", kvp.Key, kvp.Value);
                Console.WriteLine("Key = {0}, Value = {1}", kvp.Key, kvp.Value);
            }
            Console.WriteLine("###########################################");

            IAddressFinder germanAddressFinder2 = new germanAddressFinder();
            IAddressFinder universalAddressFinder2 = new UniversalAddressFinder();
            IAddressManager manager2 = new Manager(germanAddressFinder2, universalAddressFinder2);
            IAddressFinder correspondingFinder2 = manager2.GetAddressFinderByCountryCode("DE");
            IEnumerable<string> test2 = new string[] { "Liselotte Sommer", "Rhondorfer Str. 666 // Appartment 47", "50939 KÖLN", "GERMANY" };
            IDictionary<string, string> ouptutDictionary2 = correspondingFinder2.ParseAddress(test2);
            foreach (KeyValuePair<string, string> kvp2 in ouptutDictionary2)
            {
                Console.WriteLine("Key = {0}, Value = {1}", kvp2.Key, kvp2.Value);
            }
            Console.WriteLine("###########################################");

            IAddressFinder germanAddressFinder3 = new germanAddressFinder();
            IAddressFinder universalAddressFinder3 = new UniversalAddressFinder();
            IAddressManager manager3 = new Manager(germanAddressFinder3, universalAddressFinder3);
            IAddressFinder correspondingFinder3 = manager3.GetAddressFinderByCountryCode("deutschland");
            IEnumerable<string> test3 = new string[] { "Schloß Britz ", "Ortsteil Neukoelln", "Alt-Britz 73 ", "12359 BERLIN", "GERMANY" };
            IDictionary<string, string> ouptutDictionary3 = correspondingFinder3.ParseAddress(test3);
            foreach (KeyValuePair<string, string> kvp3 in ouptutDictionary3)
            {
                Console.WriteLine("Key = {0}, Value = {1}", kvp3.Key, kvp3.Value);
            }
            Console.WriteLine("###########################################");

            IAddressFinder germanAddressFinder4 = new germanAddressFinder();
            IAddressFinder universalAddressFinder4 = new UniversalAddressFinder();
            IAddressManager manager4 = new Manager(germanAddressFinder4, universalAddressFinder4);
            IAddressFinder correspondingFinder4 = manager4.GetAddressFinderByCountryCode("DE");
            IEnumerable<string> test4 = new string[] { "Harry Hase ", "Postlagernd", "53131 BONN ", "GERMANY" };
            IDictionary<string, string> ouptutDictionary4 = correspondingFinder4.ParseAddress(test4);
            foreach (KeyValuePair<string, string> kvp3 in ouptutDictionary4)
            {
                Console.WriteLine("Key = {0}, Value = {1}", kvp3.Key, kvp3.Value);
            }
            Console.WriteLine("###########################################");

            IAddressFinder germanAddressFinder5 = new germanAddressFinder();
            IAddressFinder universalAddressFinder5 = new UniversalAddressFinder();
            IAddressManager manager5 = new Manager(germanAddressFinder5, universalAddressFinder5);
            IAddressFinder correspondingFinder5 = manager5.GetAddressFinderByCountryCode("DE");
            IEnumerable<string> test5 = new string[] { "Sitftung Warentest", "Postfach 3 41 41 ", "10724 BERLIN", "GERMANY" };
            IDictionary<string, string> ouptutDictionary5 = correspondingFinder5.ParseAddress(test5);
            foreach (KeyValuePair<string, string> kvp3 in ouptutDictionary5)
            {
                Console.WriteLine("Key = {0}, Value = {1}", kvp3.Key, kvp3.Value);
            }
            Console.WriteLine("###########################################");

            IAddressFinder germanAddressFinder6 = new germanAddressFinder();
            IAddressFinder universalAddressFinder6 = new UniversalAddressFinder();
            IAddressManager manager6 = new Manager(germanAddressFinder6, universalAddressFinder6);
            IAddressFinder correspondingFinder6 = manager6.GetAddressFinderByCountryCode("DE");
            IEnumerable<string> test6 = new string[] { "Citibank Privatkunden AG ", " 68151 MANNHEIM", "GERMANY" };
            IDictionary<string, string> ouptutDictionary6 = correspondingFinder6.ParseAddress(test6);
            foreach (KeyValuePair<string, string> kvp3 in ouptutDictionary6)
            {
                Console.WriteLine("Key = {0}, Value = {1}", kvp3.Key, kvp3.Value);
            }
            Console.WriteLine("###########################################");

            IAddressFinder germanAddressFinder7 = new germanAddressFinder();
            IAddressFinder universalAddressFinder7 = new UniversalAddressFinder();
            IAddressManager manager7 = new Manager(germanAddressFinder7, universalAddressFinder7);
            IAddressFinder correspondingFinder7 = manager7.GetAddressFinderByCountryCode("DE");
            IEnumerable<string> test7 = new string[] { "Lara Lustig", "1234567", "Packstation 101", "53113 BONN ", "GERMANY" };
            IDictionary<string, string> ouptutDictionary7 = correspondingFinder7.ParseAddress(test7);
            foreach (KeyValuePair<string, string> kvp3 in ouptutDictionary7)
            {
                Console.WriteLine("Key = {0}, Value = {1}", kvp3.Key, kvp3.Value);
            }
            Console.WriteLine("######### UNIVERSAL ADDRESS FINDER ########");

            IAddressFinder germanAddressFinder8 = new germanAddressFinder();
            IAddressFinder universalAddressFinder8 = new UniversalAddressFinder();
            IAddressManager manager8 = new Manager(germanAddressFinder8, universalAddressFinder8);
            IAddressFinder correspondingFinder8 = manager8.GetAddressFinderByCountryCode("EG");
            IEnumerable<string> test8 = new string[] { "Mostafa Amer", "Flurstrasse st. 17", "Kaiserslautern 67657", "Egypt"};
            IDictionary<string, string> ouptutDictionary8 = correspondingFinder8.ParseAddress(test8);
            foreach (KeyValuePair<string, string> kvp3 in ouptutDictionary8)
            {
                Console.WriteLine("Key = {0}, Value = {1}", kvp3.Key, kvp3.Value);
            }
            Console.WriteLine("###########################################");

            IAddressFinder germanAddressFinder9 = new germanAddressFinder();
            IAddressFinder universalAddressFinder9 = new UniversalAddressFinder();
            IAddressManager manager9 = new Manager(germanAddressFinder9, universalAddressFinder9);
            IAddressFinder correspondingFinder9 = manager9.GetAddressFinderByCountryCode("EG");
            IEnumerable<string> test9 = new string[] { "Mostafa Amer", "Additional Information", "Flurstrasse st. 17", "Kaiserslautern 67657", "Egypt" };
            IDictionary<string, string> ouptutDictionary9 = correspondingFinder9.ParseAddress(test9);
            foreach (KeyValuePair<string, string> kvp3 in ouptutDictionary9)
            {
                Console.WriteLine("Key = {0}, Value = {1}", kvp3.Key, kvp3.Value);
            }
        }
    }
}
