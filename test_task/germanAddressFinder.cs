﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace test_task
{
    public class germanAddressFinder : AddressFinderSuperClass, IAddressFinder
    {
        IDictionary<string, string> IAddressFinder.ParseAddress(IEnumerable<string> addressLines)
        {
            
            int numberOfLines = addressLines.Count();
            if (numberOfLines < 3 || numberOfLines > 5)
                throw new WrongFormatException("Data entered should consist of minimum of 3 lines " +
                    "and maximum of 5 lines");
            else if (numberOfLines == 3)
            {
                addAddresse(addressLines.ElementAt(0));
                addPostalAndCountry(addressLines.ElementAt(1), addressLines.ElementAt(2));

            }
            else if (numberOfLines == 4)
            {
                handle_4_lines_of_input(addressLines, 0);
            }
            else if (numberOfLines == 5)
            {
                if (addressLines.ElementAt(0).ToLower().Equals("herrn") || addressLines.ElementAt(0).ToLower().Equals("frau"))
                {
                    handle_4_lines_of_input(addressLines, 1);
                }
                else
                {
                    base.addAddresse(addressLines.ElementAt(0));


                    if (addressLines.ElementAt(2).ToLower().Contains("packstation")) //Sending to a packstation
                    {
                        //extracting packstation number by removing the word "packstation" and any additional white spaces
                        string packstation_number = addressLines.ElementAt(2).ToLower().Replace("packstation", "").Replace(" ", String.Empty);
                        //ouptutDictionary.Add("packstation", "True");
                        ouptutDictionary.Add("packstation number", packstation_number);

                        //adding personal cutomer number
                        if (addressLines.ElementAt(1).All(char.IsDigit))
                            ouptutDictionary.Add("personal customer number", addressLines.ElementAt(1));
                        else
                            throw new WrongFormatException("Personal cusotmer number should not contain any non-numerical characters");
                    }
                    else //extract sub-locality, street name and house number instead
                    {
                        //adding sub-locality
                        if (!addressLines.ElementAt(1).All(char.IsDigit))
                            ouptutDictionary.Add("sub-locality", addressLines.ElementAt(1));
                        else
                            throw new WrongFormatException("Sub-locality should not contain any numerical characters");

                        //Adding street name and house number using method from superclass
                        addDestination(addressLines.ElementAt(2));
                    }

                    //Adding postal code and country
                    addPostalAndCountry(addressLines.ElementAt(3), addressLines.ElementAt(4));
                }
                
            }


            return ouptutDictionary;
        }

        public override void addAddresse(string name_of_addressee)
        {
            if (name_of_addressee.Any(char.IsDigit))
                throw new WrongFormatException("Name of addressee should not contain any digits");

            ouptutDictionary.Add("company name", name_of_addressee);
        }

        public void handle_4_lines_of_input(IEnumerable<string> addressLines, int i)
        {
            base.addAddresse(addressLines.ElementAt(0 + i));

            if (addressLines.ElementAt(1 + i).ToLower().Contains("postlagernd"))
                ouptutDictionary.Add("postlagernd", "True");
            else if (addressLines.ElementAt(1 + i).ToLower().Contains("postfach"))
            {
                //ouptutDictionary.Add("postfach", "True");
                string box_number;
                string postfach_line = addressLines.ElementAt(1 + i);

                postfach_line = removeWhites(postfach_line); //eleminate white spaces

                string[] postfach_split = postfach_line.Split(' ');
                string last_string = postfach_split[postfach_split.Length - 1];
                if (postfach_split[0].ToLower().Equals("postfach"))
                    box_number = addressLines.ElementAt(1 + i).Remove(0, "postfach".Length).Replace(" ", String.Empty);
                else
                    box_number = addressLines.ElementAt(1 + i).Remove(addressLines.ElementAt(1 + i).Length - "postfach".Length, "postfach".Length).Replace(" ", String.Empty);

                if (!box_number.All(char.IsDigit))
                    throw new WrongFormatException("Please remove any non-numerical characters from the P.O. box number");
                ouptutDictionary.Add("P.O. box number", box_number);
            }
            else
                addDestination(addressLines.ElementAt(1 + i));

            addPostalAndCountry(addressLines.ElementAt(2 + i), addressLines.ElementAt(3 + i));
        }

    }

}
