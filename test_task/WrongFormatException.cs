﻿using System;
namespace test_task
{
    public class WrongFormatException : System.Exception
    {
        public WrongFormatException() : base() { }
        public WrongFormatException(string message) : base(message) { }
        
    }
}
