﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace test_task
{
    public class UniversalAddressFinder : AddressFinderSuperClass, IAddressFinder
    {
        readonly int MINIMUM_OF_MANDATORY_LINES = 4;
        readonly int MAXIMUM_OF_MANDATORY_LINES = 5;

        IDictionary<string, string> IAddressFinder.ParseAddress(IEnumerable<string> addressLines)
        {

            int numberOfLines = addressLines.Count();
            if (numberOfLines < MINIMUM_OF_MANDATORY_LINES || numberOfLines > MAXIMUM_OF_MANDATORY_LINES)
            {
                throw new WrongFormatException("Data entered should consist of minimum of 4 lines " +
                    "and maximum of 5 lines");
            }
            else if (numberOfLines == MINIMUM_OF_MANDATORY_LINES) //No additional Info
            {
                addAddresse(addressLines.ElementAt(0));
                addMandatoryInfo(addressLines.ElementAt(numberOfLines - 3), addressLines.ElementAt(numberOfLines - 2), addressLines.ElementAt(numberOfLines - 1));
            }
            else //Additional Info available
            {
                addAddresse(addressLines.ElementAt(0));
                addAdditionalInfo(addressLines.ElementAt(1));
                addMandatoryInfo(addressLines.ElementAt(numberOfLines - 3), addressLines.ElementAt(numberOfLines - 2), addressLines.ElementAt(numberOfLines - 1));
            }
            return ouptutDictionary;

        }

        public void addMandatoryInfo(string line1, string line2, string line3)
        {
            //adding street and house number using method from superclass
            addDestination(line1);

            //adding postal code and country name using method from superclass
            addPostalAndCountry(line2, line3);
        }

        public void addAdditionalInfo(string additional_info)
        {
            ouptutDictionary.Add("additional info", additional_info);
        }

    }

   
}
