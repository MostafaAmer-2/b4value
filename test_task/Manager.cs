﻿using System;
namespace test_task
{
    public class Manager : IAddressManager
    {
        IAddressFinder germanAddressFinder;
        IAddressFinder universalAddressFinder;

        public Manager(IAddressFinder germanAddressFinder, IAddressFinder universalAddressFinder)
        {
            this.germanAddressFinder = germanAddressFinder;
            this.universalAddressFinder = universalAddressFinder;
        }

        public IAddressFinder GetAddressFinderByCountryCode(string countryCode)
        {
            countryCode = countryCode.ToLower();
            if (countryCode.Equals("d") || countryCode.Equals("de"))
                return germanAddressFinder;
            else
                return universalAddressFinder;
        }

        public IAddressFinder GetAddressFinderByCountryName(string countryName)
        {
            countryName = countryName.ToLower();
            if (countryName.Equals("germany") || countryName.Equals("deutschland"))
                return germanAddressFinder;
            else
                return universalAddressFinder;
        }
            
    }
}
