﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace test_task
{
    public interface IAddressManager
    {
        IAddressFinder GetAddressFinderByCountryName(string countryName);
        IAddressFinder GetAddressFinderByCountryCode(string countryCode);
    }
}
