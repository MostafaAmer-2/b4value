﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace test_task
{
    public class AddressFinderSuperClass
    {
        protected IDictionary<string, string> ouptutDictionary = new Dictionary<string, string>();

        public virtual void addAddresse(string name_of_addressee)
        {
            if (name_of_addressee.Any(char.IsDigit))
                throw new WrongFormatException("Name of addressee should not contain any digits");

            ouptutDictionary.Add("addressee", name_of_addressee);
        }

        public void addDestination(string line1)
        {
            string first_half;
            line1 = removeWhites(line1);
            if (line1.Contains("//"))
            {
                string[] house_and_street = line1.Split(new string[] { "//" }, StringSplitOptions.None);
                first_half = house_and_street[0];
                string apartment = house_and_street[house_and_street.Length - 1].Replace(" ", String.Empty);
                ouptutDictionary.Add("apartment number", apartment);
            }
            else
                first_half = line1;

            //extracting house number and street name from line1
            first_half = removeWhites(first_half);

            string[] line1_split = first_half.Split(' ');
            string last_string_line1 = line1_split[line1_split.Length - 1];
            string house_number;
            string street;

            if (last_string_line1.Any(char.IsDigit)) //house number comes at the end, thus street name at beginning
            {
                house_number = line1_split[line1_split.Length - 1];
                street = first_half.Remove((first_half.Length) - (house_number.Length+1), house_number.Length+1);
                ouptutDictionary.Add("house number", house_number);
                ouptutDictionary.Add("street name", street);
            }
            else if (line1_split[0].Any(char.IsDigit))//house number comes at the beginning, thus street name comes at end
            {
                house_number = line1_split[0];
                street = first_half.Remove(0, house_number.Length+1);
                ouptutDictionary.Add("house number", house_number);
                ouptutDictionary.Add("street name", street);
            }
            else
            {
                street = first_half;
                ouptutDictionary.Add("street name", street);
            }
            if (street.Any(char.IsDigit))
                throw new WrongFormatException("House number should be entered and separated from the street name with a white space");

            
        }

        public void addPostalAndCountry(string line1, string line2)
        {
            //Extracting postal code and city name from line1
            line1 = removeWhites(line1);

            string[] line1_split = line1.Split(' ');
            string last_string_line2 = line1_split[line1_split.Length - 1];
            string postal_code;
            string city;

            if (last_string_line2.Any(char.IsDigit)) //check if postal code comes at the end, thus city name at beginning
            {
                postal_code = line1_split[line1_split.Length - 1];
                city = line1.Remove((line1.Length) - (postal_code.Length), postal_code.Length);
            }
            else
            {
                postal_code = line1_split[0];
                city = line1.Remove(0, postal_code.Length+1);
            }

            if (!postal_code.All(char.IsDigit))
                throw new WrongFormatException("Please remove any non-numerical charcaters from the postal code"); //postal code should consist of numerical digits only (that's why 'All' was used)

            if (city.Any(char.IsDigit))
                throw new WrongFormatException("Postal code should be entered and separated from the city name with a white space");

            ouptutDictionary.Add("postcode", postal_code);
            ouptutDictionary.Add("locality", city);

            //line2 contains only country name
            line2 = removeWhites(line2);
            string country = line2;
            if (country.Any(char.IsDigit))
            {
                throw new WrongFormatException("Please enter a valid country name by removing any numbers associated with the name");
            }
            ouptutDictionary.Add("country", country);
        }

        public string removeWhites(string st)
        {
            while (st[st.Length - 1] == ' ') //to remove white spaces at the end
                st = st.Substring(0, st.Length - 1);
            while (st[0] == ' ') //to remove white spaces at the beginning
                st = st.Substring(1, st.Length - 1);
            return st;
        }
    }
}
